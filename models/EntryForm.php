<?php

namespace app\models;

use Yii;
use yii\base\Model;

class EntryForm extends Model
{
	public $name;
	public $email;
	public $checkbox;
	public $captcha;

	public function rules()
	{
		return [
			[['name', 'email'], 'required'],
			['captcha', 'captcha'],
			['checkbox', 'boolean'],
			['email', 'email']
		];
	}
}