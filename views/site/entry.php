<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\captcha\Captcha;
?>
<?php $form = ActiveForm::begin(); ?>

<?= $form->field($model, 'name')->label('Name') ?>

<?= $form->field($model, 'email')->label('Email') ?>

<?= $form->field($model, 'checkbox')->checkbox() ?>

<?= $form->field($model, 'captcha')->widget(Captcha::className()) ?>

<div class="form-group">
	<?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
</div>

<?php ActiveForm::end(); ?>