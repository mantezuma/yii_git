<?php
use yii\helpers\Html;
?>
<p>You have entered the following information:</p>

<ul>
	<li><label>Name</label>: <?= Html::encode(Yii::$app->cache['name']) ?></li>
	<li><label>Email</label>: <?= Html::encode(Yii::$app->cache['email']) ?></li>
	<li><label>Checkbox</label>: <?= Html::encode(Yii::$app->cache['checkbox'] ? 'on' : 'off') ?></li>
</ul>